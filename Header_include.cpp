// Including the header files
#include <iostream>
#include "Header_files.h"

// Running the main function
int main()
{
  // Given two numbers
  int a = 13, b = 22;

  // Displaying the results
  std::cout << "The sum is: " << sumOfTwoNumbers(a, b) << std::endl;

  // Closing the program
  return 0;
}
