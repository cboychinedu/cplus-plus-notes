// 
#include <iostream> 
#include <string> 
#include "main_module.h" 


// Running the main function 
int main()
{
    // Calling the function inside the "main_module" header file 
    sum_numbers(30, 40); 

    // closing the program 
    return 0; 
}